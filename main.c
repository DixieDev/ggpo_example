#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <SDL.h>
#include <stdio.h>
#include <time.h>

#include <ggponet.h>

#define WIN_WIDTH 640
#define WIN_HEIGHT 480

#define NS_PER_SEC 1000000000
#define TICK_RATE 60
#define GG_IDLE_MS 10

#define MOVE_SPEED 4

// Frame Data for our characters' only attack. Currently, this is the same as
// Ryu's crouching medium kick in Street Fighter V.
#define STARTUP 7
#define ACTIVE 3
#define RECOVERY 13

#define CHR_Y 380
#define CHR_WIDTH 96
#define CHR_HEIGHT 194
#define ATK_WIDTH 128
#define ATK_HEIGHT 64

#define LOG(str_) printf((str_)); fflush(stdout)
#define LOGF(fmt_, ...) printf((fmt_), __VA_ARGS__); fflush(stdout)

// Simple enum for character's state. They can do whatever they want when IDLE 
// or WALKING, but will be locked in when ATTACKING. Once DEAD, there can be no 
// more state changes.
enum CharacterState 
{
    CHR_IDLE,
    CHR_WALKING,
    CHR_ATTACKING,
    CHR_DEAD
};

// Just tells us which side of the screen a character is on. We use this to 
// decide where to put their attack's hitbox.
enum CharacterSide 
{
    CHR_RIGHT,
    CHR_LEFT
};

// The actual character.
struct Character 
{
    enum CharacterState state;
    enum CharacterSide side;
    int x;

    // These timers are used when attacking to control the flow: 
    // startup -> active -> recovery -> IDLE
    int atk_startup_timer;
    int atk_active_timer;
    int atk_recovery_timer;

    // These are the colliders. The atk_box is only present during active and 
    // recovery frames. During active frames, the atk_box will kill the 
    // opponent, but during recovery it is vulnerable to enemy attacks.
    SDL_Rect body_box;
    SDL_Rect atk_box;
};

// This is our input information
struct Input 
{
    bool left_held;
    bool right_held;
    bool atk_pressed;
};

// This is the whole game state
struct Game 
{
    struct Character p1, p2;
};

// Checks if two rects are overlapping
bool check_overlap(SDL_Rect a, SDL_Rect b) 
{
    return a.x <= b.x+b.w
        && a.x+a.w >= b.x
        && a.y <= b.y+b.h
        && a.y+a.h >= b.y;
}

// Initialises a character with a bunch of default values
struct Character create_character(int x, enum CharacterSide side) 
{
    struct Character chr;

    chr.state = CHR_IDLE;
    chr.side = side;
    chr.x = x;

    chr.atk_startup_timer = 0;
    chr.atk_active_timer = 0;
    chr.atk_recovery_timer = 0;

    chr.body_box.x = x - CHR_WIDTH/2;
    chr.body_box.y = CHR_Y - CHR_HEIGHT;
    chr.body_box.w = CHR_WIDTH;
    chr.body_box.h = CHR_HEIGHT;

    chr.atk_box.x = side == CHR_LEFT 
        ? x + CHR_WIDTH/2 
        : x - CHR_WIDTH/2 - ATK_WIDTH;
    chr.atk_box.y = CHR_Y - CHR_HEIGHT/2 - ATK_HEIGHT/2;
    chr.atk_box.w = ATK_WIDTH;
    chr.atk_box.h = ATK_HEIGHT;

    return chr;
}

// GGPO uses a lot of callbacks, but they don't take any user-data (which is a 
// common approach to passing stuff around in C) so we have to have some kind 
// of global access to the GGPOSession, our game state, and any other details
// we want to update or look at in the callbacks.
//
// In this case, we have a pointer to our Game state and the GGPOSession, a
// simple timer that will be set when we need to wait for the other player's
// game to catch-up if we desync, and a bool that tells us whether the game 
// should run, which is false until both players connect and synchronise.
struct 
{
    struct Game *game;
    GGPOSession *ggpo;
    int sync_timer;
    bool game_started;
} gg_info;

// This ticks an individual character to handle input, states, and to update 
// their position on the screen. We don't detect any collisions here, we do 
// that in the tick_game function, which is also where this gets called from.
void tick_character(struct Character *chr, struct Input input) 
{
    switch (chr->state)
    {
        case CHR_IDLE:
        case CHR_WALKING:
            // Player can attack or move around from IDLE/WALKING states
            if (input.atk_pressed) 
            {
                chr->state = CHR_ATTACKING;
                chr->atk_startup_timer = STARTUP;
            } 
            else 
            {
                int velocity = 0;
                if (input.right_held) velocity += MOVE_SPEED;
                if (input.left_held) velocity -= MOVE_SPEED;

                if (velocity == 0) 
                {
                    chr->state = CHR_IDLE;
                } 
                else 
                {
                    chr->state = CHR_WALKING;
                    chr->x += velocity;

                    if (chr->x < CHR_WIDTH/2)
                        chr->x = CHR_WIDTH/2;

                    if (chr->x > WIN_WIDTH-CHR_WIDTH/2)
                        chr->x = WIN_WIDTH-CHR_WIDTH/2;

                    chr->body_box.x = chr->x - CHR_WIDTH/2;

                    chr->atk_box.x = chr->side == CHR_LEFT 
                        ? chr->x + CHR_WIDTH/2 
                        : chr->x - CHR_WIDTH/2 - ATK_WIDTH;
                }
            }
            break;

        case CHR_ATTACKING:
            // Transition from startup -> active -> recovery -> idle
            if (chr->atk_startup_timer > 0) 
            {
                chr->atk_startup_timer -= 1;
                if (chr->atk_startup_timer == 0) 
                {
                    chr->atk_active_timer = ACTIVE;
                }
            } 
            else if (chr->atk_active_timer > 0) 
            {
                chr->atk_active_timer -= 1;
                if (chr->atk_active_timer == 0) 
                {
                    chr->atk_recovery_timer = RECOVERY;
                }
            } 
            else if (chr->atk_recovery_timer > 0) 
            {
                chr->atk_recovery_timer -= 1;
                if (chr->atk_recovery_timer == 0) 
                {
                    chr->state = CHR_IDLE;
                }
            }
            break;

        case CHR_DEAD:
            // Disable any ongoing attack 
            chr->atk_startup_timer = 0;
            chr->atk_active_timer = 0;
            chr->atk_recovery_timer = 0;
            break;

        default: 
            break;
    }
}

// This draws a character's body box on-screen. Its colour depends on the 
// character's current state.
void draw_chr(SDL_Renderer *renderer, struct Character *chr) 
{
    switch (chr->state) {
        case CHR_WALKING:
            SDL_SetRenderDrawColor(renderer, 60, 196, 196, 255);
            break;
        case CHR_ATTACKING:
            SDL_SetRenderDrawColor(renderer, 255, 119, 119, 255);
            break;
        case CHR_DEAD:
            SDL_SetRenderDrawColor(renderer, 119, 119, 119, 255);
            break;
        default:
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            break;
    }

    SDL_Rect rect = chr->body_box;
    SDL_RenderDrawRect(renderer, &rect);
}

// This draw's a character's atk_box on screen. Its colour is red during active 
// frames, and white during recovery.
void draw_chr_attack(SDL_Renderer *renderer, struct Character *chr) 
{
    if (chr->atk_active_timer > 0) 
    {
        SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
        SDL_RenderDrawRect(renderer, &chr->atk_box);
    }
    else if(chr->atk_recovery_timer > 0)
    {
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        SDL_RenderDrawRect(renderer, &chr->atk_box);
    }
}

// This ticks the whole game; ticking each character and handling collisions 
void tick_game(struct Game *game, struct Input p1_input, struct Input p2_input) 
{
    // Tick each character
    tick_character(&game->p1, p1_input);
    tick_character(&game->p2, p2_input);

    // Both die if they either overlap or trade because I can't be arsed to 
    // handle collisions properly
    bool players_touching = game->p1.state != CHR_DEAD
        && game->p2.state != CHR_DEAD
        && check_overlap(game->p1.body_box, game->p2.body_box);

    bool players_trading = game->p1.atk_active_timer > 0 
        && game->p2.atk_active_timer > 0
        && check_overlap(game->p1.atk_box, game->p2.atk_box);

    if (players_touching || players_trading) 
    {
        game->p1.state = CHR_DEAD;
        game->p2.state = CHR_DEAD;
    }

    // If p1 attacks and hits p2, p2 dies
    if (game->p1.atk_active_timer > 0)
    {
        bool hit_body = check_overlap(game->p1.atk_box, game->p2.body_box);
        bool whiff_punish = game->p2.atk_recovery_timer > 0 
                && check_overlap(game->p1.atk_box, game->p2.atk_box);

        if (hit_body || whiff_punish)
        {
            game->p2.state = CHR_DEAD;
        }
    }

    // If p2 attacks and hits p1, p1 dies
    if (game->p2.atk_active_timer > 0)
    {
        bool hit_body = check_overlap(game->p2.atk_box, game->p1.body_box);
        bool whiff_punish = game->p1.atk_recovery_timer > 0 
                && check_overlap(game->p2.atk_box, game->p1.atk_box);

        if (hit_body || whiff_punish)
        {
            game->p1.state = CHR_DEAD;
        }
    }
}

// This just draws each character and their atk_box
void draw_game(SDL_Renderer *renderer, struct Game *game) 
{
    draw_chr(renderer, &game->p1);
    draw_chr(renderer, &game->p2);
    draw_chr_attack(renderer, &game->p1);
    draw_chr_attack(renderer, &game->p2);
}

// Now I define all the callbacks. Note a couple of them don't really do 
// anything. They can do things and you may or may not find this useful in 
// your own game, but if you don't need them you still have to implement 
// them and return true!
bool __cdecl gg_begin_game(const char *deprecated)
{
    LOG("begin game\n");
    return true;
}

// State saving. In this case, we just allocate some space and copy our game 
// state into it. In a real game with more state to save, you will likely not
// want to malloc on the go, and instead implement some allocator of your own
// that you can store in something like my global gg_info struct.
bool __cdecl gg_save_game_state(unsigned char **buffer, int *len, int *checksum, int frame)
{
    *len = sizeof(struct Game);
    *buffer = (unsigned char *) malloc(*len);
    if (!*buffer)
    {
        return false;
    }

    memcpy(*buffer, gg_info.game, *len);
    return true;
}

// Load the game state... Just copies the buffer data back over our game state.
bool __cdecl gg_load_game_state(unsigned char *buffer, int len)
{
    memcpy(gg_info.game, buffer, len);
    return true;
}

// This could help debugging, you're meant to log your game state in a human 
// readable format so you can look at make sure it looks good.
bool __cdecl gg_log_game_state(char *filename, unsigned char *buffer, int len)
{
    return true;
}

// Frees our buffer. If you had your own allocator you could handle this
// differently, but since I just used malloc() I must also use free()
void __cdecl gg_free_buffer(void *buffer)
{
    free(buffer);
}

// This is called to catch-up after rolling back. It just gets the frame's 
// inputs from GGPO and ticks our game using them.
bool __cdecl gg_advance_frame(int flags)
{
    struct Input inputs[2];
    if (GGPO_SUCCEEDED(ggpo_synchronize_input(gg_info.ggpo, inputs, sizeof(inputs), NULL)))
    {
        tick_game(gg_info.game, inputs[0], inputs[1]);
        ggpo_advance_frame(gg_info.ggpo);
        return true;
    }

    return false;
}

// There are a few extra events that can occur, such as players connecting or 
// disconnecting, desyncing, etc. You can handle them here.
//
// Of note, how you handle the GGPO_EVENTCODE_TIMESYNC (desync) event may vary 
// from how I have. I just stall the game until the peer catches up. Also, 
// when a player disconnects I just kill both characters. Ragequitting OP
bool __cdecl gg_on_event(GGPOEvent *event)
{
    // TODO
    switch (event->code)
    {
        case GGPO_EVENTCODE_CONNECTED_TO_PEER:
            LOG("Connected\n");
            break;
        case GGPO_EVENTCODE_RUNNING:
            LOG("Synchronised with all peers\n");
            gg_info.game_started = true;
            break;
        case GGPO_EVENTCODE_DISCONNECTED_FROM_PEER:
            LOG("Disconnected\n");
            gg_info.game->p1.state = CHR_DEAD;
            gg_info.game->p2.state = CHR_DEAD;
            break;
        case GGPO_EVENTCODE_TIMESYNC:
            gg_info.sync_timer = event->u.timesync.frames_ahead;
            LOGF("Out of sync: Waiting %d frames for peer to catch up\n", gg_info.sync_timer);
            break;
        default:
            break;
    }
    return true;
}

// This will tell us how much time has passed since a given time, in 
// nanoseconds
long elapsed_time(struct timespec prev_time)
{
    struct timespec cur_time;
    timespec_get(&cur_time, TIME_UTC);

    time_t delta_secs = cur_time.tv_sec - prev_time.tv_sec;
    return ((long)delta_secs * NS_PER_SEC) + (cur_time.tv_nsec - prev_time.tv_nsec);
}

// Print's a message telling the user how to start the game properly
void print_usage_string()
{
    fprintf(stderr, "Usage:\n\tfootsies.exe [PLAYER 1 or 2] [LOCAL PORT] [REMOTE IP] [REMOTE PORT]\n");
}

// Here's our entry-point and main func, it sets up our game, sets up GGPO,
// sets up SDL, handles inputs, handles timings, and ticks the game. It's
// pretty big but shouldn't be too hard to read.
int main(int argc, char **argv) 
{
    // Have to initialise WinSock API ourselves. This is pretty weird 
    // considering GGPO deals with it in every other capacity, but maybe it's
    // nicer this way on bigger games that already have some networked nonsense
    // built into them.
    WSADATA wd = { 0 };
    WSAStartup(MAKEWORD(2, 2), &wd);

    // Here I just check the command line parameters to find out which player 
    // we're controlling. 
    if (argc < 5) 
    {
        print_usage_string();
        return 1;
    }

    int player_idx;
    if (argv[1][0] == '1') 
    {
        player_idx = 0;
    }
    else if (argv[1][0] == '2') 
    {
        player_idx = 1;
    }
    else 
    {
        fprintf(stderr, "Please specify which player you are (1 or 2)");
        return 2;
    }

    // Set up the game state with player characters
    struct Game game;
    game.p1 = create_character(200, CHR_LEFT);
    game.p2 = create_character(440, CHR_RIGHT);

    // Initialise input data
    struct Input p1_input = {0};
    struct Input p2_input = {0};

    // Pointers to the input data for our local player, and the remote player
    struct Input *player_input = player_idx == 0 ? &p1_input : &p2_input;
    struct Input *enemy_input = player_idx == 0 ? &p2_input : &p1_input;

    // Init our gg_info data
    gg_info.game = &game;
    gg_info.sync_timer = 0;
    gg_info.game_started = false;
    
    // Set our GGPO callbacks
    GGPOErrorCode gg_result;
    GGPOSessionCallbacks gg_callbacks;
    gg_callbacks.begin_game = gg_begin_game;
    gg_callbacks.save_game_state = gg_save_game_state;
    gg_callbacks.load_game_state = gg_load_game_state;
    gg_callbacks.log_game_state = gg_log_game_state;
    gg_callbacks.free_buffer = gg_free_buffer;
    gg_callbacks.advance_frame = gg_advance_frame;
    gg_callbacks.on_event = gg_on_event;

    // Start our GGPO session
    gg_result = ggpo_start_session(
            &gg_info.ggpo,
            &gg_callbacks,
            "GGPO Footsies",
            2,
            sizeof(struct Input), 
            atoi(argv[2])
    );

    if (!GGPO_SUCCEEDED(gg_result))
    {
        fprintf(stderr, "Failed to start GGPO session. Error code is %d\n", gg_result);
        return 3;
    }

    // Register GGPO players. 
    // TODO: IPs are hardcoded localhost... Maybe change that?
    GGPOPlayer p1, p2;
    GGPOPlayerHandle player_handles[2];
    p1.size = p2.size = sizeof(GGPOPlayer);
    p1.player_num = 1;
    p2.player_num = 2;
    if (player_idx == 0)
    {
        p1.type = GGPO_PLAYERTYPE_LOCAL;

        p2.type = GGPO_PLAYERTYPE_REMOTE;
        strcpy_s(p2.u.remote.ip_address, 32, argv[3]);
        p2.u.remote.port = atoi(argv[4]);
    }
    else 
    {
        p2.type = GGPO_PLAYERTYPE_LOCAL;

        p1.type = GGPO_PLAYERTYPE_REMOTE;
        strcpy_s(p1.u.remote.ip_address, 32, argv[3]);
        p1.u.remote.port = atoi(argv[4]);
    }

    gg_result = ggpo_add_player(gg_info.ggpo, &p1, &player_handles[0]);
    if (!GGPO_SUCCEEDED(gg_result))
    {
        fprintf(stderr, "Failed to register player 1. Error code is %d\n", gg_result);
        return 4;
    }

    gg_result = ggpo_add_player(gg_info.ggpo, &p2, &player_handles[1]);
    if (!GGPO_SUCCEEDED(gg_result))
    {
        fprintf(stderr, "Failed to register player 2. Error code is %d\n", gg_result);
        return 5;
    }

    // Init SDL, create window, create renderer
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window *window = SDL_CreateWindow(
            "GGPO Footsies",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            WIN_WIDTH, WIN_HEIGHT,
            0
    );

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

    // Init game loop
    struct timespec ftime;
    timespec_get(&ftime, TIME_UTC);

    long nanos_per_tick = NS_PER_SEC / TICK_RATE;
    long tick_timer = 0;
    bool active = true;
    while (active) 
    {
        // Handle events
        SDL_Event event;
        while (SDL_PollEvent(&event)) 
        {
            switch (event.type) 
            {
                case SDL_QUIT: 
                    active = false;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) 
                    {
                        case SDLK_RIGHT:
                            player_input->right_held = true;
                            break;
                        case SDLK_LEFT:
                            player_input->left_held = true;
                            break;
                        case SDLK_SPACE:
                            player_input->atk_pressed = true;
                            break;
                        default:
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch (event.key.keysym.sym) 
                    {
                        case SDLK_RIGHT:
                            player_input->right_held = false;
                            break;
                        case SDLK_LEFT:
                            player_input->left_held = false;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        // GGPO does most of its work during the time we grant it by calling
        // ggpo_idle(). I've considered a couple of approaches, but honestly
        // just giving it up to a flat number of milliseconds per frame seems 
        // to work best.
        ggpo_idle(gg_info.ggpo, GG_IDLE_MS);

        if (gg_info.game_started)
        {
            // Update tick_timer if the game is running
            tick_timer += elapsed_time(ftime);
        }
        timespec_get(&ftime, TIME_UTC);

        while (tick_timer >= nanos_per_tick)
        {
            // Skip a tick if we need to delay our frame (too far ahead of the 
            // peer)
            if (gg_info.game_started && gg_info.sync_timer > 0) 
            {
                gg_info.sync_timer -= 1;
            }

            // Otherwise tick the game
            else if (gg_info.game_started && gg_info.sync_timer <= 0) 
            {
                // We write our local input to GGPO so it can send it to our 
                // enemy
                gg_result = ggpo_add_local_input(
                        gg_info.ggpo,
                        player_handles[player_idx],
                        player_input,
                        sizeof(struct Input)
                );
                if (GGPO_SUCCEEDED(gg_result))
                {
                    // We get the inputs synchronised by GGPO for this frame 
                    // and use those to tick our game
                    struct Input inputs[2];
                    gg_result = ggpo_synchronize_input(gg_info.ggpo, inputs, sizeof(inputs), NULL);
                    if (GGPO_SUCCEEDED(gg_result))
                    {
                        tick_game(&game, inputs[0], inputs[1]);
                        p1_input.atk_pressed = false;
                        p2_input.atk_pressed = false;
                        ggpo_advance_frame(gg_info.ggpo);
                    }
                }
            }

            tick_timer -= nanos_per_tick;
        }

        // Draw the game
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        draw_game(renderer, &game);

        SDL_RenderPresent(renderer);
    }

    // Clear up after ourselves
    ggpo_close_session(gg_info.ggpo);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
