#!/bin/bash

clang main.c \
    -o footsies.exe \
    -I SDL2-2.0.10/include -I ggpo/include \
    -L SDL2-2.0.10/lib/x64 -L ggpo/lib/x64 \
    -l GGPO \
    -l SDL2main \
    -l SDL2 \
    -l ws2_32 \
    -Xlinker //subsystem:windows

